import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';
import { ProductsService } from "./products/products.service";
import { SearchResultsComponent } from './products/search-results/search-results.component';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';



import { AppComponent } from './app.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { LoginComponent } from './login/login.component';
import { ProductsComponent } from './products/products.component';
import { NavigationComponent } from './navigation/navigation.component';
import { EditProductComponent } from './products/edit-product/edit-product.component';

@NgModule({
  declarations: [
    AppComponent,
    NotFoundComponent,
    LoginComponent,
    ProductsComponent,
    NavigationComponent,
    SearchResultsComponent,
    EditProductComponent

  ],
  imports: [
    BrowserModule,
    HttpModule,
     FormsModule,
   ReactiveFormsModule,
    RouterModule.forRoot([
    {path: '', component: ProductsComponent},
    {path: 'products', component: ProductsComponent},
    {path: 'edit-product/:id', component: EditProductComponent},
    {path: 'login', component: LoginComponent},
    {path: '**', component: NotFoundComponent}
          ])
 ],
  
  providers: [ProductsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
