import {Injectable } from '@angular/core';
import{Http,Headers} from '@angular/http';
import{HttpParams} from '@angular/common/http';

@Injectable()
export class ProductsService {
http:Http;


getProducts(){
 return this.http.get('http://localhost/slim/products');
          }
  getProduct(id){
     return this.http.get('http://localhost/slim/products/'+ id);
  }

   putProduct(data,key){
    let options = {
      headers: new Headers({
       'content-type':'application/x-www-form-urlencoded'
      })
    }
    //בגרש נגדיר את השדות בהתאם לערכים שהגדרנו בשרת
    let params = new HttpParams().append('name',data.name).append('price',data.price);
    return this.http.put('http://localhost/slim/products/'+ key,params.toString(), options);
  }

   updateProduct(id,product){
    let options = {
      headers: new Headers({'content-type': 'application/x-www-form-urlencoded'}
    )};
    var params = new HttpParams().append('name',product.name).append('price',product.price);
    return this.http.put('http://localhost/slim/products/'+ id, params.toString(), options);      
  }

  
  

constructor(http:Http){
  this.http = http;
  }


}
