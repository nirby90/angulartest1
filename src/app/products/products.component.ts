
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Router } from "@angular/router";
import { ProductsService } from "./products.service";


@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
 products;
productsKeys;
product_service:ProductsService;
 searchForm = new FormGroup({
 name:new FormControl()
  });
   sendData(){
 }
 
  constructor(private service:ProductsService) {
 service.getProducts().subscribe(
      response=>{
        this.products = response.json();
        this.productsKeys = Object.keys(this.products);
  });
  }

  ngOnInit() {
  }

}
