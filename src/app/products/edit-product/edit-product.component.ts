import { Component, OnInit, Output , EventEmitter } from '@angular/core';
import { ProductsService } from "../../products/products.service";
import {FormGroup , FormControl, FormBuilder} from '@angular/forms';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.css']
})
export class EditProductComponent implements OnInit {

@Output() updateProduct:EventEmitter<any> = new EventEmitter<any>(); 
@Output() updateProductPs:EventEmitter<any> = new EventEmitter<any>();

name;
price;

service:ProductsService;

product;

updateform = new FormGroup({
name:new FormControl(),
price:new FormControl()

});

  constructor(private route: ActivatedRoute ,service: ProductsService, private formBuilder: FormBuilder, private router: Router) { 
    this.service = service;
  }
          sendData() {
          this.updateProduct.emit(this.updateform.value.name);
          console.log(this.updateform.value);

          this.route.paramMap.subscribe(params=>{
            let id = params.get('id');
            this.service.putProduct(this.updateform.value, id).subscribe(
              response => {
                console.log(response.json());
                this.updateProductPs.emit();
                this.router.navigate(['/']);
              }
            );
          })
        }
        
          ngOnInit() {
            this.route.paramMap.subscribe(params=>{
              let id = params.get('id');
              console.log(id);
              this.service.getProduct(id).subscribe(response=>{
                this.product = response.json();
                console.log(this.product);
                this.name = this.product.name
                this.price = this.product.price  
              })
            })
          }
}
